class EnquiryMailer < ApplicationMailer

  def leads(enquiry)
    @enquiry = enquiry
    if @enquiry.building.present?
    mail(:to => "info@evhive.co", :subject => "New leads coming from #{@enquiry.name} for #{@enquiry.building.name}") do |format|
      format.text { render text: "New leads:\n\nName: #{@enquiry.name} #{@enquiry.last_name}.\nPhone: #{@enquiry.phone}\nEmail: #{@enquiry.email}\nNotes:#{@enquiry.enquiry}\n\nThank You \n[SYSTEM] EV HIVE" }
    end
    else
      mail(:to => "info@evhive.co", :subject => "New leads coming from #{@enquiry.name} ") do |format|
        format.text { render text: "New leads:\n\nName: #{@enquiry.name} #{@enquiry.last_name}.\nPhone: #{@enquiry.phone}\nEmail: #{@enquiry.email}\nNotes:#{@enquiry.enquiry}\n\nThank You \n[SYSTEM] EV HIVE" }
      end
    end
  end

  def thanks(enquiry)
    @enquiry = enquiry
    mail(:to => "#{@enquiry.email}", :subject => "[EV Hive] Thank you #{@enquiry.name}, for your enquiry") do |format|
      format.text { render text: "Hello #{@enquiry.name} #{@enquiry.last_name}.\n\nThank you for contacting us.\nWe will get back to you immediately\n\nThank You \nEV HIVE" }
    end
  end

  def notify_facility(subscription)
    @subscription = subscription
    @facility = Manager.where('building_id = ? and manager is true', @subscription.building_id)
    mail(:to => "#{@facility.collect(&:email).join(", ")}", :subject => "[IMPORTANT] Please generate invoice at evhive.info") do |format|
       format.text { render text: "Please generate report/invoice for the below details.\nName: #{@subscription.user.fullname}\nCompany: #{@subscription.user.fullname}\nStart and end date: Today\n generate the transaction at evhive.info.\n\nThank You \nSYSTEM EV HIVE" }
    end
  end

end
