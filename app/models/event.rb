class Event < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: :slugged

  has_attached_file :banner, styles: { medium: "300x300>", thumb: "100x100>" }, :path => ":rails_root/public/images/files/:class/:attachment/:id/:style/:filename",:url => "/images/files/:class/:attachment/:id/:style/:filename"
  validates_attachment_content_type :banner, content_type: /\Aimage\/.*\z/
end
