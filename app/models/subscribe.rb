class Subscribe < ActiveRecord::Base
  after_create :mailchimp_checker

	def mailchimp_checker
		if self.email.present?
			self.send_mailchimp
		end
	end

	def send_mailchimp
		require 'uri'
		require 'net/http'

		url = URI("https://us12.api.mailchimp.com/3.0/lists/4aca4ebd2b/members")

		http = Net::HTTP.new(url.host, url.port)
		http.use_ssl = true
		http.verify_mode = OpenSSL::SSL::VERIFY_NONE

		@payload = {
			"email_address": email,
			"status": "pending"
		}.to_json

		request = Net::HTTP::Post.new(url)
		request.basic_auth 'anystring', '088e65406e89502544ca8720a0026c7e-us12'
		
		request.body = @payload
		request["content-type"] = 'application/json'
		
		response = http.request(request)
    puts response.read_body
	end

end
