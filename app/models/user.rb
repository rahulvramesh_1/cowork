class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :subscriptions, :dependent => :destroy
  accepts_nested_attributes_for :subscriptions,  :reject_if => :all_blank, :allow_destroy => true
  after_create :send_qontak
  
  def send_qontak
    require 'uri'
    require 'net/http'

    url = URI("https://www.qontak.com/api/evhive/v1/crm/deals/deal")

    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    @payload = {
      "first_name": fullname,
      "last_name": "",
      "phone_number": "",
      "email": email,
      "building": self.subscriptions.last.plan.building.name,
      "enquiry": "Sign up"
    }.to_json

    request = Net::HTTP::Post.new(url)
    request.basic_auth '72ccdbcddda1ace3c8253318671f1106', '7534206357eb40e52ec2a9be79ba0daf59d02d0f5bfc813b6c2eec4e3c154593'
    request["content-type"] = 'application/json'
    request.body = @payload

    response = http.request(request)
    puts response.read_body
  end
end
