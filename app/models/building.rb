class Building < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :slugged

  belongs_to :location
  has_many :plans
  has_many :rooms

  has_attached_file :image, styles: { medium: "300x300>", thumb: "100x100>" },    :path => ":rails_root/public/images/manager/:class/:attachment/:id/:style/:filename",:url => "/images/manager/:class/:attachment/:id/:style/:filename"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/

  has_many :banners, inverse_of: :building
  accepts_nested_attributes_for :banners, reject_if: :all_blank, allow_destroy: true

end
