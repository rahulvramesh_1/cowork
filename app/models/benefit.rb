class Benefit < ActiveRecord::Base
  has_attached_file :logo, styles: { medium: "300x300>", thumb: "100x100>" }, :path => ":rails_root/public/images/files/:class/:attachment/:id/:style/:filename",:url => "/images/files/:class/:attachment/:id/:style/:filename"
  validates_attachment_content_type :logo, content_type: /\Aimage\/.*\z/
end
