class Subscription < ActiveRecord::Base
  after_save :deduct_plan
  after_create :check_payment
  belongs_to :user
  belongs_to :plan
  belongs_to :payment
  has_one :payment_log
  before_update :send_data

  def send_data
    if self.payment.name == "Bank Transfer"
      self.middleware_bridge
    end
  end

  def deduct_plan
    plan = Plan.find(plan_id)
    plan.update_attributes(:quantity => (plan.quantity - 1))
  end

  def check_payment
    if self.payment.name == "Bank Transfer"
      payment = Payment.find(payment_id)
      payment.update_attributes(:api => "1")
    elsif self.payment.name == "Virtual Account"
      xendit_va
    else
      #self.middleware_bridge
    end
  end

  def xendit_va
    url = URI("https://api.xendit.co/v2/invoices")
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    @payload = {"external_id" => self.id.to_s , "payer_email" => self.user.email, "description" =>  self.plan.name , "amount" => self.plan.price.to_int }

    request = Net::HTTP::Post.new(url.path)
    if Rails.env.production?
      request.basic_auth 'xnd_production_O4+AfOUl1rf5kc87ebAYEzHBM9Kj9dF9xnK2+R1q+WHV8LClBQ53gw==', ''
    else
      request.basic_auth 'xnd_development_O46GfOUl1rf5kc87ebAYEzHBM9Kj9dF9xnK2+R1q+WHV8LClBQ50hQ==', ''
    end
    request["content-type"] = 'application/json'

    request.body = @payload.to_json

    response = http.request(request)

    if response.code == "200"
      parsed_response = JSON.parse(response.read_body)
      src = parsed_response["id"]
      va_status = parsed_response["status"]
      x_url = parsed_response["invoice_url"]

      xendit_log(src,va_status,x_url)
    end

  end

  def xendit_log(src,va_status,x_url)
    paymentlog = PaymentLog.create(:payment_id => self.payment_id, :status => va_status, :src_id => src, :subscription_id => id, :url => x_url)
    paymentlog.save!

  end

  def middleware_bridge
    require 'uri'
    require 'net/http'

    if Rails.env.production?
      url = URI("https://evhive.info/api/v1/suborders/")
    else
      url = URI("http://mware.dev/api/v1/suborders/")
    end

    http = Net::HTTP.new(url.host, url.port)
    if Rails.env.production?
    http.use_ssl = true
    else
    http.use_ssl = false
    end
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    @payload = {
      "start_date": Date.today,
      "end_date": Date.today.next_month,
      "total": plan.price,
      "days": "1",
      "product_name": plan.jurnal_name,
      "company": user.fullname,
      "email": user.email,
      "center": plan.building.tag
    }.to_json

    request = Net::HTTP::Post.new(url)
    request["content-type"] = 'application/json'
    request.body = @payload

    response = http.request(request)
    puts response.read_body
  end

end
