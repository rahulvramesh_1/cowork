class Schedule < ActiveRecord::Base
  belongs_to :room
  belongs_to :user
  validate :booking_period_not_overlapped

  def realtime_check
    if self.start_time < Time.now and self.end_time > Time.now
      return true
    else
      return false
    end
  end

  def self.room_checker
    val =  self.where('start_time < ? and end_time > ?', Time.now, Time.now).count

    return val
  end

  def duration
    ((self.end_time - self.start_time) / 3600).round
  end

  def booking_period_not_overlapped

    st = Schedule.where('(room_id = ? AND start_time >= ? AND start_time <= ?)',room_id,start_time,end_time)
    mt = Schedule.where('(room_id = ? AND start_time <= ? AND end_time >= ?)',room_id,start_time,end_time)
    et = Schedule.where('(room_id = ? AND end_time >= ? AND end_time <= ?)',room_id,start_time,end_time)

    if !st.empty?
      errors.add(:error, ': Time Unavailable')
      return false
    elsif !mt.empty?
      errors.add(:error, ': Time Unavailable')
      return false
    elsif !et.empty?
      errors.add(:error, ': Time Unavailable')
      return false
    else
      return true
    end

  end


end
