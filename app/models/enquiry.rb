class Enquiry < ActiveRecord::Base
  attr_accessor :source
  belongs_to :building
  after_create :q_checker

  def q_checker
    if self.name.present?
      self.send_qontak
    end
  end

  def send_qontak
    require 'uri'
    require 'net/http'

    url = URI("https://www.qontak.com/api/evhive/v1/crm/leads/contact")

    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    
    if building.present?
      @payload = {
        "first_name": name,
        "last_name": last_name,
        "phone_number": phone,
        "email": email,
        "building": building.name,
        "source": self.source,
        "enquiry": plan,
      }.to_json
    end

    request = Net::HTTP::Post.new(url)
    request.basic_auth '72ccdbcddda1ace3c8253318671f1106', '7534206357eb40e52ec2a9be79ba0daf59d02d0f5bfc813b6c2eec4e3c154593'
    request["content-type"] = 'application/json'
    request.body = @payload

    response = http.request(request)
    puts response.read_body
  end
end
