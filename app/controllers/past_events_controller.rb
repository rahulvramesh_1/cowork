class PastEventsController < ApplicationController
  def index
    @pasts = Event.where('date < ?', Date.today).order(date: :desc);
  end
end
