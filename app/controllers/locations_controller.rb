class LocationsController < ApplicationController
  before_action :set_location, only: [:show]
  def index
    @locations =  Location.all
  end

  def show
    @buildings = Building.where("location_id = ?", params[:id])
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_location
      @location = Location.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def location_params
      params.require(:location).permit(:name)
    end
end
