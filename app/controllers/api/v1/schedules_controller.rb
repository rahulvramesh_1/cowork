class Api::V1::SchedulesController < Api::V1::BaseController
  respond_to :json

  def index
    headers['Access-Control-Allow-Origin'] = "*"
    room_name = params[:room_name]
    building =  params[:building_id]
    if room_name
      respond_with Schedule.joins(:room).where('rooms.name = ? and rooms.building_id = ?', room_name, building).map{|f|{:title => 'Occupied', :start => f.start_time, :end => f.end_time}}
    else
      respond_with Schedule.all
    end
  end

  def show
    respond_with Schedule.find(params[:id])
  end

  def create
    @schedules = Schedule.new(schedule_params)

    if @schedules.save
      render json: @schedules, status: 201
    else
      render json: { errors: @schedules.errors}, status: 422
    end
  end

  private

  def schedule_params
    params.require(:schedule).permit(:start_time, :end_time, :room_id, :user_id)
  end

end
