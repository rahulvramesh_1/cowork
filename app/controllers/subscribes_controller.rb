class SubscribesController < ApplicationController
	def new
		@subscribe = Subscribe.new
	end

	def create
		@subscribe = Subscribe.new(subscribe_params)

		if @subscribe.save
			redirect_to root_path, notice:'Successfully Subscribed!'
		else
			redirect_to root_path, notice: 'Please enter your email again'
		end
	end

	private
	def subscribe_params
		params.require(:subscribe).permit(:email)
	end
end
