class PlansController < ApplicationController
  before_action :set_plan, only: [:show, :edit, :update, :destroy]
  def index
    if params[:location_id]
      @plans = Plan.joins(:building).where("buildings.location_id = ?", params[:location_id])
    elsif params[:building_id] && params[:name]
      @plans = Plan.where("building_id = ? and name = ?", params[:building_id], params[:name])
    else
      @plans = Plan.all
    end
  end
  def show
  end

  def meeting_room
  end

  def event_space
    @buildings = Building.all
  end

  def creative_studio
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_plan
    @plan = Plan.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def plan_params
    params.require(:plan).permit(:name, :description, :price, :building_id)
  end
end
