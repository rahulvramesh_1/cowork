class HomeController < ApplicationController
  def index
    @futures = Event.where('date >= ?', Date.today).order(date: :desc)
  end

  def about
  end

  def student_ambassador
  end

  def creative_studio
  end

  def privacy_policy
  end

  def thank_you

  end
end
