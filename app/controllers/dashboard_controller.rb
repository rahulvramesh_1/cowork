class DashboardController < ApplicationController
  def index
    @subscriptions = Subscription.where("user_id = ?", current_user.id).order(created_at: :desc)
    @benefits = Benefit.all
  end
end
