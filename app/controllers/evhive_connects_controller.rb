class EvhiveConnectsController < ApplicationController
  before_action :set_evhive_connect, only: [:show, :edit, :update, :destroy]

  # GET /evhive_connects
  # GET /evhive_connects.json
  def index
    @evhive_connects = EvhiveConnect.all
    @evhive_connect = EvhiveConnect.new
  end

  # GET /evhive_connects/1
  # GET /evhive_connects/1.json
  def show
  end

  # GET /evhive_connects/new
  def new
    @evhive_connect = EvhiveConnect.new
  end

  # GET /evhive_connects/1/edit
  def edit
  end

  # POST /evhive_connects
  # POST /evhive_connects.json
  def create
    @evhive_connect = EvhiveConnect.new(evhive_connect_params)

    respond_to do |format|
      if @evhive_connect.save
        format.html { redirect_to evhive_connects_path, notice: 'Evhive connect user was successfully created.' }
        format.json { render :index, status: :created, location: evhive_connects_path }
      else
        format.html { render :index }
        format.json { render json: @evhive_connect.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /evhive_connects/1
  # PATCH/PUT /evhive_connects/1.json
  def update
    respond_to do |format|
      if @evhive_connect.update(evhive_connect_params)
        format.html { redirect_to @evhive_connect, notice: 'Evhive connect was successfully updated.' }
        format.json { render :show, status: :ok, location: @evhive_connect }
      else
        format.html { render :edit }
        format.json { render json: @evhive_connect.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /evhive_connects/1
  # DELETE /evhive_connects/1.json
  def destroy
    @evhive_connect.destroy
    respond_to do |format|
      format.html { redirect_to evhive_connects_url, notice: 'Evhive connect was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_evhive_connect
      @evhive_connect = EvhiveConnect.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def evhive_connect_params
      params.require(:evhive_connect).permit(:name, :email, :startup, :need)
    end
end
