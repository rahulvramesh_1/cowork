class PaymentsController < ApplicationController
  before_action :set_payment, only: [:show, :edit, :update, :destroy]

  def new
    @payment = Payment.new
  end

  def create
    @payment = Payment.new(payment_params)

    respond_to do |format|
      if @payment.save
        format.html { redirect_to managers_payments_path, notice: 'Payment was successfully created.' }
        format.json { render :show, status: :created, location: managers_payments_path }
      else
        format.html { render :new }
        format.json { render json: @payment.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET /payments
  # GET /payments.json

  # GET /payments/1
  # GET /payments/1.json
  def show
  end

  def edit
  end

  def update
    respond_to do |format|
      if @payment.update(payment_params)
        format.html { redirect_to managers_payments_path, notice: 'Payment was successfully updated.' }
        format.json { render :show, status: :ok, location: managers_payments_path }
      else
        format.html { render :edit }
        format.json { render json: @payment.errors, status: :unprocessable_entity }
      end
    end
  end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_payment
      @payment = Payment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def payment_params
      params.require(:payment).permit(:name, :description)
    end
end
