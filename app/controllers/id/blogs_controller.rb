class Id::BlogsController < ApplicationController
	before_action :set_blog, only: [:show]
  def index
		@blogs = Blog.where('lang = ? AND press = ?', 'bahasa', false).order(id: :desc)
	end

	def show
	
	end

	private

	def set_blog
		@blog = Blog.find(params[:id])
	end
end
