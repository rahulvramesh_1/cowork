class Managers::DashboardController < ApplicationController
  layout 'manager'
  
  before_filter :authenticate_manager!

  def index
  end
end
