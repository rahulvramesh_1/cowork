class Managers::EventsController < EventsController
  layout 'manager'
  before_filter :authenticate_manager!

  def index
    @events = Event.all
  end


end
