class Managers::EnquiriesController < EnquiriesController
  before_filter :authenticate_manager!
  layout 'manager'
  
  def index
    @enquiries = Enquiry.where('name is not ?', nil)
  end
end
