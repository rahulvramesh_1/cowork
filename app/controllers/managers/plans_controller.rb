class Managers::PlansController < ApplicationController
  layout 'manager'

  before_filter :authenticate_manager!
  before_action :set_plan, only: [:show, :edit, :update, :destroy]

  # GET /plans.json
  def index
    if current_manager.building_id.nil?
      @plans = Plan.all
    else
      @plans = Plan.where('building_id = ?', current_manager.building_id)
    end
  end

  # GET /plans/1
  # GET /plans/1.json
  def show
  end

  # GET /plans/new
  def new
    @plan = Plan.new
  end

  # GET /plans/1/edit
  def edit
  end

  # POST /plans
  # POST /plans.json
  def create
    @plan = Plan.new(plan_params)

    respond_to do |format|
      if @plan.save
        format.html { redirect_to [:managers,@plan], notice: 'Plan was successfully created.' }
        format.json { render :show, status: :created, location: [:managers,@plan] }
      else
        format.html { render :new }
        format.json { render json: @plan.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /plans/1
  # PATCH/PUT /plans/1.json
  def update
    respond_to do |format|
      if @plan.update(plan_params)
        format.html { redirect_to [:managers,@plan], notice: 'Plan was successfully updated.' }
        format.json { render :show, status: :ok, location: [:managers,@plan] }
      else
        format.html { render :edit }
        format.json { render json: @plan.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /plans/1
  # DELETE /plans/1.json
  def destroy
    @plan.destroy
    respond_to do |format|
      format.html { redirect_to admins_plans_url, notice: 'Plan was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_plan
    @plan = Plan.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def plan_params
    params.require(:plan).permit(:name, :description, :price, :building_id, :quantity, :jurnal_name)
  end
end
