class Managers::BooksController < BooksController
  def index
    if current_manager.building_id.nil?
      @books = Book.all
    else
      @books = Book.where('building_id = ?', current_manager.building_id)
    end
  end
end
