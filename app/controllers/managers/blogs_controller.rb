class Managers::BlogsController < BlogsController
  layout 'manager'
  
  before_filter :authenticate_manager!
  def index
    @blogs = Blog.where('lang = ?', 'english')
    @blogs_id= Blog.where('lang = ?', 'bahasa')
    @press= Blog.where('press = ?', true)
  end
end
