class Managers::EvhiveConnectsController < EvhiveConnectsController
  layout 'manager'
  before_filter :authenticate_manager!

  def index
    @evhive_connects = EvhiveConnect.all
  end
end
