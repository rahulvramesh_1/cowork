class Managers::PaymentsController < PaymentsController
	layout 'manager'
  
	before_filter :authenticate_manager!
	
	def index
		@payments = Payment.all
	end
end
