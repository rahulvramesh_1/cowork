class Managers::SchedulesController < SchedulesController
  layout 'manager'
  before_filter :authenticate_manager!

  def index
    if current_manager.building_id.nil?
      @schedules = Schedule.all
    else
      @schedules = Schedule.joins(:room).where('rooms.building_id = ?',current_manager.building_id)
    end
  end

  def new
    @schedule = Schedule.new
  end

  def create
    @schedule = Schedule.new(schedule_params)
    @schedule.user_id = current_user.id
    @schedule.start_time = Time.parse(params[:schedule][:start_time]) + 1.minutes
    @schedule.end_time = Time.parse(params[:schedule][:end_time])  - 1.minutes

    roomid = @schedule.room_id
    respond_to do |format|
      if @schedule.save
        format.html { redirect_to managers_schedules_path, notice: 'Schedule was successfully created.' }
        format.json { render :show, status: :created, location: managers_schedules_path}
      else
        # flash[:error] = @schedule.errors
        format.html { redirect_to new_managers_schedule_path, notice: 'Time unavailable'}
        # format.json { render json: @schedule.errors, status: :unprocessable_entity }
      end
    end
  end

  private

    def schedule_params
      params.require(:schedule).permit(:start_time, :end_time, :room_id, :user_id)
    end
end
