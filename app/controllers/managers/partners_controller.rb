class Managers::PartnersController < PartnersController
  layout 'manager'
  before_filter :authenticate_manager!

  def index
    @partners = Partner.all
  end
end
