class Managers::RoomsController < RoomsController
  layout 'manager'


  def index
    if current_manager.building_id.nil?
      @rooms = Room.all
    else
      @rooms = Room.where('building_id = ?', current_manager.building_id)
    end
  end

end
