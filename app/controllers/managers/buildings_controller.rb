class Managers::BuildingsController < ApplicationController
  layout 'manager'

  before_filter :authenticate_manager!
  before_action :set_building, only: [:show, :edit, :update, :destroy]

  # GET /plans.json
  def index
    @buildings = Building.all
  end

  # GET /plans/1
  # GET /plans/1.json
  def show
  end

  # GET /plans/new
  def new
    @building = Building.new
    @building.banners.build
  end

  # GET /plans/1/edit
  def edit
    @building.banners.build
  end

  # POST /plans
  # POST /plans.json
  def create
    @building = Building.new(building_params)

    respond_to do |format|
      if @building.save
        format.html { redirect_to [:managers,@building], notice: 'Plan was successfully created.' }
        format.json { render :show, status: :created, location: [:managers,@building] }
      else
        format.html { render :new }
        format.json { render json: @building.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /plans/1
  # PATCH/PUT /plans/1.json
  def update
    respond_to do |format|
      if @building.update(building_params)
        format.html { redirect_to [:managers,@building], notice: 'Plan was successfully updated.' }
        format.json { render :show, status: :ok, location: [:managers,@building] }
      else
        format.html { render :edit }
        format.json { render json: @building.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /plans/1
  # DELETE /plans/1.json
  def destroy
    @building.destroy
    respond_to do |format|
      format.html { redirect_to managers_buildings_url, notice: 'Plan was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_building
    @building = Building.friendly.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def building_params
    params.require(:building).permit(:name, :address, :phone, :map, :description,:available, :meta_title, :meta_description, :tag, :location_id,:image, :spin_link, :parking_fee, banners_attributes: [:id, :image, :_destroy])
  end
end
