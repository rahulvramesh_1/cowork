class Managers::LocationsController < ApplicationController
  layout 'manager'
  
  before_filter :authenticate_manager!
  before_action :set_location, only: [:show, :edit, :update, :destroy]

  # GET /plans.json
  def index
    @locations = Location.all
  end

  # GET /plans/1
  # GET /plans/1.json
  def show
  end

  # GET /plans/new
  def new
    @location = Location.new
  end

  # GET /plans/1/edit
  def edit
  end

  # POST /plans
  # POST /plans.json
  def create
    @location = Location.new(location_params)

    respond_to do |format|
      if @location.save
        format.html { redirect_to [:managers,@location], notice: 'Plan was successfully created.' }
        format.json { render :show, status: :created, location: [:managers,@location] }
      else
        format.html { render :new }
        format.json { render json: @location.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /plans/1
  # PATCH/PUT /plans/1.json
  def update
    respond_to do |format|
      if @location.update(location_params)
        format.html { redirect_to [:managers,@location], notice: 'Plan was successfully updated.' }
        format.json { render :show, status: :ok, location: [:managers,@location] }
      else
        format.html { render :edit }
        format.json { render json: @location.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /plans/1
  # DELETE /plans/1.json
  def destroy
    @location.destroy
    respond_to do |format|
      format.html { redirect_to managers_locations_url, notice: 'Plan was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_location
    @location = Location.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def location_params
    params.require(:location).permit(:name, :image)
  end
end
