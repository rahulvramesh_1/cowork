class Managers::BenefitsController < BenefitsController
  layout 'manager'
  before_filter :authenticate_manager!
  def index
    @benefits = Benefit.all
  end
end
