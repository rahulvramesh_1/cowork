class Managers::EvhiveClassesController < EvhiveClassesController
  layout 'manager'
  before_filter :authenticate_manager!

  def index
    @evhive_classes = EvhiveClass.all
  end
end
