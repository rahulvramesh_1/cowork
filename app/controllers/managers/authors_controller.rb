class Managers::AuthorsController < AuthorsController
  layout 'manager'
  before_filter :authenticate_manager!
  @authors = Author.all
end
