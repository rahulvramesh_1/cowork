class Managers::ReportController < ApplicationController
  layout 'manager'
  before_action :set_subscription, only: [:show, :destroy]
  before_filter :authenticate_manager!
  def index
    if current_manager.building_id.nil?
      @subscriptions = Subscription.where('paid is ?', nil)
      @psubs = Subscription.where('paid = ?', true)
    else
      @subscriptions = Subscription.where('building_id = ? and paid is ?', current_manager.building_id, nil)
      @psubs = Subscription.where('building_id = ? and paid = ?', current_manager.building_id, true)
    end
  end

  def show
  end

  def destroy
    @subscription.destroy
    respond_to do |format|
      format.html { redirect_to managers_report_index_path, notice: 'Subscription was successfully destroyed.' }
      format.json { head :no_content }
    end
  end


  def paid
    @subscription = Subscription.find(params[:id])
    @subscription.update_attribute :paid, true
    respond_to do |format|
      EnquiryMailer.notify_facility(@subscription).deliver_now
      format.html { redirect_to managers_report_index_path, notice: "success" }
    end
  end

  private

  def set_subscription
    @subscription = Subscription.find(params[:id])
  end
end
