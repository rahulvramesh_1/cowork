class Managers::PromosController < PromosController
  layout 'manager'
  before_filter :authenticate_manager!

  def index
    @promos = Promo.all
  end
end
