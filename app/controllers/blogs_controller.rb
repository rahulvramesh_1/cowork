class BlogsController < ApplicationController
  before_action :set_blog, only: [:show, :edit, :update, :destroy]

  def index
    if params[:tag]
     @blogs = Blog.tagged_with(params[:tag]).order(id: :desc)
    else
     @blogs = Blog.where('lang = ? AND press = ?','english',false).order(id: :desc)
    end
  end

  def new
    @blog = Blog.new
  end

  def show
    #@related = Tagging.where("blog_id = ?",@blog.tag_id)
    @tagging = Tagging.where('blog_id = ?',@blog.id).map{|i| i.tag_id}
    @tag = Tagging.where('tag_id in (?)',@tagging).map{|j| j.blog_id}
    @related = Blog.where('id in (?)', @tag)
  end

  def create
    @blog = Blog.new(blog_params)

    respond_to do |format|
      if @blog.save
        format.html { redirect_to managers_blogs_path, notice: 'Blog was successfully created.' }
        format.json { render :show, status: :created, location: managers_blogs_path }
      else
        format.html { render :new }
        format.json { render json: @blog.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
  end

  def update
    respond_to do |format|
      if @blog.update(blog_params)
        format.html { redirect_to managers_blogs_path, notice: 'Blog was successfully updated.' }
        format.json { render :show, status: :ok, location: managers_blogs_path }
      else
        format.html { render :edit }
        format.json { render json: @blog.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @blog.destroy
    respond_to do |format|
      format.html { redirect_to managers_blogs_path, notice: 'Blog was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def press
    @blogs = Blog.where('press =?',true).order(id: :desc)
  end
  private

  def set_blog
    @blog = Blog.friendly.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def blog_params
    params.require(:blog).permit(:title, :meta_description, :content, :all_tags, :banner, :lang, :press, :author_id)
  end
end
