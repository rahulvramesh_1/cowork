class EvhiveClassesController < ApplicationController
  before_action :set_evhive_class, only: [:show, :edit, :update, :destroy]

  # GET /evhive_classes
  # GET /evhive_classes.json
  def index
    @evhive_classes = EvhiveClass.all
    @partners = Partner.all
    @categories = Category.all
  end

  # GET /evhive_classes/1
  # GET /evhive_classes/1.json
  def show
  end

  # GET /evhive_classes/new
  def new
    @evhive_class = EvhiveClass.new
  end

  # GET /evhive_classes/1/edit
  def edit
  end

  # POST /evhive_classes
  # POST /evhive_classes.json
  def create
    @evhive_class = EvhiveClass.new(evhive_class_params)

    respond_to do |format|
      if @evhive_class.save
        format.html { redirect_to managers_evhive_classes_path, notice: 'Evhive class was successfully created.' }
        format.json { render :show, status: :created, location: managers_evhive_classes_path }
      else
        format.html { render :new }
        format.json { render json: @evhive_class.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /evhive_classes/1
  # PATCH/PUT /evhive_classes/1.json
  def update
    respond_to do |format|
      if @evhive_class.update(evhive_class_params)
        format.html { redirect_to managers_evhive_classes_path, notice: 'Evhive class was successfully updated.' }
        format.json { render :show, status: :ok, location: managers_evhive_classes_path }
      else
        format.html { render :edit }
        format.json { render json: @evhive_class.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /evhive_classes/1
  # DELETE /evhive_classes/1.json
  def destroy
    @evhive_class.destroy
    respond_to do |format|
      format.html { redirect_to managers_evhive_classes_path, notice: 'Evhive class was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_evhive_class
      @evhive_class = EvhiveClass.friendly.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def evhive_class_params
      params.require(:evhive_class).permit(:name, :description, :get, :need, :instructor, :price, :included, :agenda, :location, :partner_id,:category_id,:image, :link)
    end
end
