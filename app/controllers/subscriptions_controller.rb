class SubscriptionsController < ApplicationController
  before_action :set_subscription, only: [:show, :edit, :update, :destroy,:thank_you]

  def new
    @subscription = Subscription.new
  end

  def create
    @subscription = Subscription.new(subscription_params)
    @subscription.user_id = current_user.id
    respond_to do |format|
      if @subscription.save
        # decrement dedicated desk
        if @subscription.plan.name == "Dedicated Desk"
          @plan = Plan.find(@subscription.plan_id)
          @plan.update_attributes(:quantity => (@plan.quantity - 1))
          @plan.save
        end

        format.html { redirect_to @subscription, notice: 'Plan was successfully created.' }
        format.json { render :show, status: :created, location: @subscription }
      else
        format.html { render :new }
        format.json { render json: @subscription.errors, status: :unprocessable_entity }
      end
    end
  end

  def show
    if @subscription.paid
      redirect_to sub_thank_subscriptions_path  
    end
  end

  def thank_you
  end

  private

  def set_subscription
    @subscription = Subscription.find(params[:id])
  end

  def subscription_params
    params.require(:subscription).permit(:user_id, :plan_id, :payment_id, :building_id)
  end
end
