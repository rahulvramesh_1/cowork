json.extract! benefit, :id, :name, :description, :validity, :created_at, :updated_at
json.url benefit_url(benefit, format: :json)
