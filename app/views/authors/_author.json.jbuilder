json.extract! author, :id, :name, :email, :about, :blog_id, :avatar, :created_at, :updated_at
json.url author_url(author, format: :json)
