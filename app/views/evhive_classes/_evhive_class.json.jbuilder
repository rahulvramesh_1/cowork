json.extract! evhive_class, :id, :name, :description, :get, :need, :instructor, :price, :included, :minimum, :type, :partner_id, :created_at, :updated_at
json.url evhive_class_url(evhive_class, format: :json)
