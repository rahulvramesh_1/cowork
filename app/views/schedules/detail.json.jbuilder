# json.array! @schedules, partial: 'schedules/schedule', as: :schedule

json.array!(@schedules) do |schedule|
  json.extract! schedule, :id
  json.title "occupied"
  json.start schedule.start_time
  # if current_user.id == booking.user_id
  #   json.color '#6cd86a'
  # else
  #   json.color '#f39c12'
  # end
  # if booking.description == "purchase"
  #   json.color 'red'
  # end
  json.end schedule.end_time
  json.url schedule_url(schedule, format: :html)
end
