json.extract! plan, :id, :name, :price, :building_id, :created_at, :updated_at
json.url plan_url(plan, format: :json)
