json.extract! promo, :id, :name, :description, :price, :created_at, :updated_at
json.url promo_url(promo, format: :json)
