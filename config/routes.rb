Rails.application.routes.draw do

  get 'sitemap.xml', :to => 'sitemap#index', :defaults => {:format => 'xml'}

  get 'upcoming_events/index'

  get 'past_events/index'

  resources :schedules
  get "schedules/detail/:room_id" => "schedules#detail", as: :detail

  namespace :api, defaults: { format: :json }  do
    namespace :v1 do
      resources :schedules, :only => [:index,:show, :create, :update]
      post "receive/xendit" => "receive#xendit"
      get "receive/xendit" => "receive#xendit"
    end
  end

  namespace :managers do
    get 'books/index'
  end

  namespace :id do
    resources :blogs, :only => [:index, :show]
  end

  mount_roboto
  mount Ckeditor::Engine => '/ckeditor'
  resources :benefits, :only => [:index, :show]
  resources :careers, :only => [:index, :show]
  resources :schoolpreneurs, controller:'evhive_classes', :only => [:index, :show]
  resources :evhive_connects, :only => [:index,:create]
  resources :authors, :only => [:show]
  resources :partners, :only => [:index, :show]
  resources :events, :only => [:index, :show]
  resources :blogs, :only => [:index, :show]
  resources :categories, :only => [:show]
  resources :promos, :only => [:index, :show]
  resources :subscribes, :only => [:create]
  get 'tags/:tag', to: 'blogs#index', as: "tag"

  resources :books
  resources :payments
  resources :enquiries
  namespace :managers do
    root to: 'dashboard#index'
    resources :plans, :buildings, :locations, :blogs,:categories, :events, :benefits, :rooms,:report, :dashboard, :books, :careers, :partners, :evhive_classes, :evhive_connects, :schedules, :enquiries, :promos, :payments, :authors
    get "report/paid/:id" => "report#paid", as: :set_paid
    get "report/destroy/:id" => "report#destroy", as: :set_destroy
  end

  devise_for :managers
  resources :subscriptions do
    collection do
      get 'thank_you/:id' => "subscriptions#thank_you", as: :sub_thank
    end
  end
  resources :dashboard

  devise_for :users, controllers: {
    :registrations => 'users/registrations',
    :sessions => "users/sessions"
  }
  resources :locations
  resources :buildings
  resources :plans

  get 'about' => 'home#about'
  get 'press' => 'blogs#press'
  get 'enquiry' => 'enquiries#new'
  get 'privacy_policy' => 'home#privacy_policy'
  get 'thank_you' => 'home#thank_you'
  get 'student_ambassador' => 'home#student_ambassador'
  get 'creative-studio', to: 'plans#creative_studio'
  get 'meeting-room', to: 'plans#meeting_room'
  get 'event-space', to: 'plans#event_space'
  get '/past_events' => 'past_events#index'
  root to: 'home#index'

end
