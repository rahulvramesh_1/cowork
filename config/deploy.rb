lock '3.8.1'

set :application, 'evhive'
set :repo_url, 'git@bitbucket.org:kidi_sh/cowork.git'
set :deploy_to, '/home/deploy/evhive'

append :linked_files, 'config/database.yml', 'config/secrets.yml'
append :linked_dirs,'bin', 'log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle','public/system', 'public/images', 'public/ckeditor_assets'

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:web), in: :sequence, wait: 5 do
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  after :publishing, 'deploy:restart'
  after :finishing, 'deploy:cleanup'
end
