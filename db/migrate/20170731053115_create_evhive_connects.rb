class CreateEvhiveConnects < ActiveRecord::Migration
  def change
    create_table :evhive_connects do |t|
      t.string :name
      t.string :email
      t.string :startup
      t.string :need

      t.timestamps null: false
    end
  end
end
