class RemoveBlogIdFromAuthors < ActiveRecord::Migration
  def change
    remove_column :authors, :blog_id, :integer
  end
end
