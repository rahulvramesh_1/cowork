class AddAvailableToBuildings < ActiveRecord::Migration
  def change
    add_column :buildings, :available, :boolean
  end
end
