class CreatePaymentLogs < ActiveRecord::Migration
  def change
    create_table :payment_logs do |t|
      t.integer :payment_id
      t.string :status
      t.string :src_id
      t.integer :subscription_id

      t.timestamps null: false
    end
  end
end
