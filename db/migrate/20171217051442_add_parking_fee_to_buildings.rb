class AddParkingFeeToBuildings < ActiveRecord::Migration
  def change
    add_column :buildings, :parking_fee, :text
  end
end
