class AddSlugToEvhiveClasses < ActiveRecord::Migration
  def change
    add_column :evhive_classes, :slug, :string
    add_index :evhive_classes, :slug, unique: true
  end
end
