class AddMetaToBuildings < ActiveRecord::Migration
  def change
    add_column :buildings, :meta_title, :string
    add_column :buildings, :meta_description, :string
  end
end
