class CreateBooks < ActiveRecord::Migration
  def change
    create_table :books do |t|
      t.integer :user_id
      t.integer :building_id
      t.datetime :start
      t.datetime :end

      t.timestamps null: false
    end
  end
end
