class AddLangToBlogs < ActiveRecord::Migration
  def change
    add_column :blogs, :lang, :string
  end
end
