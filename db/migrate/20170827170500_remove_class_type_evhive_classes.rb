class RemoveClassTypeEvhiveClasses < ActiveRecord::Migration
  def change
    remove_column :evhive_classes, :class_type, :string
    remove_column :evhive_classes, :minimum, :string
  end
end
