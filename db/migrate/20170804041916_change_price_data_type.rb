class ChangePriceDataType < ActiveRecord::Migration
  def up
    change_column :evhive_classes, :price, :text
  end
  
  def down
    change_column :evhive_classes, :price, :decimal
  end
end
