class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.integer :plan_id
      t.integer :payment_id
      t.integer :user_id
      t.integer :building_id

      t.timestamps null: false
    end
  end
end
