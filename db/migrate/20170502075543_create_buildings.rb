class CreateBuildings < ActiveRecord::Migration
  def change
    create_table :buildings do |t|
      t.string :name
      t.string :address
      t.string :phone
      t.text :description
      t.integer :location_id

      t.timestamps null: false
    end
  end
end
