class AddFullnameToManager < ActiveRecord::Migration
  def change
    add_column :managers, :fullname, :string
    add_column :managers, :building_id, :integer
  end
end
