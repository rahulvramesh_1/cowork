class CreateEvhiveClasses < ActiveRecord::Migration
  def change
    create_table :evhive_classes do |t|
      t.string :name
      t.text :description
      t.text :get
      t.text :need
      t.string :instructor
      t.decimal :price
      t.string :included
      t.string :minimum
      t.string :type
      t.belongs_to :partner, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
