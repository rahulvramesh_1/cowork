class AddLinkToEvhiveClass < ActiveRecord::Migration
  def change
    add_column :evhive_classes, :link, :string
  end
end
