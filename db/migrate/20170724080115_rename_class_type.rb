class RenameClassType < ActiveRecord::Migration
  def change
    rename_column :evhive_classes, :type, :class_type
  end
end
