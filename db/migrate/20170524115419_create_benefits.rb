class CreateBenefits < ActiveRecord::Migration
  def change
    create_table :benefits do |t|
      t.string :name
      t.text :description
      t.datetime :validity

      t.timestamps null: false
    end
  end
end
