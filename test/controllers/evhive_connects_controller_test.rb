require 'test_helper'

class EvhiveConnectsControllerTest < ActionController::TestCase
  setup do
    @evhive_connect = evhive_connects(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:evhive_connects)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create evhive_connect" do
    assert_difference('EvhiveConnect.count') do
      post :create, evhive_connect: { email: @evhive_connect.email, name: @evhive_connect.name, need: @evhive_connect.need, startup: @evhive_connect.startup }
    end

    assert_redirected_to evhive_connect_path(assigns(:evhive_connect))
  end

  test "should show evhive_connect" do
    get :show, id: @evhive_connect
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @evhive_connect
    assert_response :success
  end

  test "should update evhive_connect" do
    patch :update, id: @evhive_connect, evhive_connect: { email: @evhive_connect.email, name: @evhive_connect.name, need: @evhive_connect.need, startup: @evhive_connect.startup }
    assert_redirected_to evhive_connect_path(assigns(:evhive_connect))
  end

  test "should destroy evhive_connect" do
    assert_difference('EvhiveConnect.count', -1) do
      delete :destroy, id: @evhive_connect
    end

    assert_redirected_to evhive_connects_path
  end
end
