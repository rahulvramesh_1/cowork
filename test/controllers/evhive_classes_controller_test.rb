require 'test_helper'

class EvhiveClassesControllerTest < ActionController::TestCase
  setup do
    @evhive_class = evhive_classes(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:evhive_classes)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create evhive_class" do
    assert_difference('EvhiveClass.count') do
      post :create, evhive_class: { description: @evhive_class.description, get: @evhive_class.get, included: @evhive_class.included, instructor: @evhive_class.instructor, minimum: @evhive_class.minimum, name: @evhive_class.name, need: @evhive_class.need, partner_id: @evhive_class.partner_id, price: @evhive_class.price, type: @evhive_class.type }
    end

    assert_redirected_to evhive_class_path(assigns(:evhive_class))
  end

  test "should show evhive_class" do
    get :show, id: @evhive_class
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @evhive_class
    assert_response :success
  end

  test "should update evhive_class" do
    patch :update, id: @evhive_class, evhive_class: { description: @evhive_class.description, get: @evhive_class.get, included: @evhive_class.included, instructor: @evhive_class.instructor, minimum: @evhive_class.minimum, name: @evhive_class.name, need: @evhive_class.need, partner_id: @evhive_class.partner_id, price: @evhive_class.price, type: @evhive_class.type }
    assert_redirected_to evhive_class_path(assigns(:evhive_class))
  end

  test "should destroy evhive_class" do
    assert_difference('EvhiveClass.count', -1) do
      delete :destroy, id: @evhive_class
    end

    assert_redirected_to evhive_classes_path
  end
end
