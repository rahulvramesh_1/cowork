require 'test_helper'

class PartsControllerTest < ActionController::TestCase
  test "should get _nav" do
    get :_nav
    assert_response :success
  end

  test "should get _footer" do
    get :_footer
    assert_response :success
  end

end
